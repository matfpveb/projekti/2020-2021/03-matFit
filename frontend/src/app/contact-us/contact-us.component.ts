import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, ValidationErrors, Validators } from '@angular/forms';
import { ContactNameValidator } from '../validators/contact-name-validator';
import { ContactUs } from '../models/contactUs.model';


@Component({
  selector: 'app-contact-us',
  templateUrl: './contact-us.component.html',
  styleUrls: ['./contact-us.component.css']
})


export class ContactUsComponent implements OnInit {


  createContactForm: FormGroup;
  public contact: ContactUs;

  constructor(private formBuilder: FormBuilder) {


    this.createContactForm = this.formBuilder.group({
      name: ['', [Validators.required, Validators.minLength(3), ContactNameValidator]],
      surname: ['', [Validators.required, Validators.minLength(2)]],
      email: ['', [Validators.required, Validators.minLength(10)]],
      message: ['', [Validators.required, Validators.minLength(20)]]
    });
   }

  ngOnInit(): void {
  }

  public send():void{

    if(this.createContactForm.invalid){
      window.alert("Please enter all fields");
    }
    else{
      window.alert("Your message is sent! Thank you :)");
      window.location.reload();
    }


  }

  public emailHasErrors(): boolean{
    const errors: ValidationErrors = this.createContactForm.get("email").errors;

    return errors != null;
  }

  public emailErrors(): string[]{
    const errors: ValidationErrors = this.createContactForm.get("email").errors;
    if(errors === null){
      return [];
    }

    const errorMessages: string[] = [];
    if(errors.required){
      errorMessages.push("Don't forget to enter your email!");
    }

    if(errors.minlength){
      errorMessages.push('Email adress must have at least' + errors.minlength.requiredLength + 'characters');
    }

    if(errors.contactName){
      errorMessages.push(errors.contactName.message);
    }

    return errorMessages;

  }

  public messageHasErrors(): boolean {
    const errors: ValidationErrors = this.createContactForm.get("message").errors;

    return errors != null;
  }

  public messageErrors(): string[]{
    const errors: ValidationErrors = this.createContactForm.get("message").errors;
    if(errors === null){
      return [];
    }

    const errorMessages: string[] = [];
    if(errors.required){
      errorMessages.push("Don't forget to enter the message! We'd like to hear from you. ");
    }

    if(errors.minlength){
      errorMessages.push('Message must have at least' + errors.minlength.requiredLength + 'characters');
    }

    if(errors.contactName){
      errorMessages.push(errors.contactName.message);
    }

    return errorMessages;

  }

}
