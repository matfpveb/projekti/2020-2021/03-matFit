export class User {
    public constructor(
        public id: string,
        public username: string,
        public email: string,
        public firstName: string,
        public lastName: string,
        public gender: string,
        public age: number, 
        public height: number,
        public weight: number
    ){}

}

