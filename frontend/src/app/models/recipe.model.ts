export class Recipe {

    constructor(
        public name: string,
        public creator: string,
        public ingredients: Array<string>,
        public prep_time_mins: number,
        public difficulty: string,
        public instructions: Array<string>,
        public category: Array<string>,
        public image_url: string,
        public nutrients: Nutrients,
        public meal: string,
        public _id: string,
        ) {}

}

export class Nutrients {
    constructor(
        public kcal: number,
        public protein: number,
        public carbs: number,
        public fats: number,
    ){}
}


export enum CategoryEnum{
    Anything = 'anything',
    GlutenFree = 'gluten-free',
    DairyFree = 'dairy-free',
    EggFree = 'egg-free',
    Vegan = 'vegan',
    Vegetarian = 'vegetarian',
    Healthy = 'healthy',
    HighProtein = 'high-protein',
    Sweet = 'sweet',
    Salad = 'salad',
    Paleo = 'paleo',
    Keto = 'keto',
    Empty = ''
    }


export enum MealEnum{
    Breakfast = 'breakfast',
    Lunch = 'lunch',
    Dinner = 'dinner',
    Desert = 'desert',
    Snack = 'snack',
    Empty = '',
    Any = 'any'

}

export enum DifficultyEnum{
    Easy = "easy",
    MoreEffort = "more effort",
    Empty = '',
    Any = 'any'
}

