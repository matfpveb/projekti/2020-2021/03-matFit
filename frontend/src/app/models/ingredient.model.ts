export enum ProductPopularity {
    Popular = 'Popular'
}

export class Ingredient {
    constructor(
      public name: string,
      public kcal: number,
      public protein: number,
      public carbohydrates: number,
      public sugar: number,
      public fat: number,
      public saturated: number,
      public cholesterol: number,
    ){}

    get popularity(): ProductPopularity{
        return ProductPopularity.Popular;
    }
}