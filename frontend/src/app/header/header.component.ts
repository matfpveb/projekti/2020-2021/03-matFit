import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { User } from '../models/user.model';
import { UserService } from '../services/user.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit, OnDestroy {
  private sub: Subscription;
  public user: User = null;

  constructor(private userService: UserService) { 
  }

  ngOnInit(): void {
    this.sub = this.userService.user.subscribe((user: User)=>{
      this.user = user;
      console.log(this.user);
    });
  }

  ngOnDestroy(): void{
    if(this.sub){
      this.sub.unsubscribe();
    }
  }

  isLoggedIn(){
    return this.userService.isLoggedIn();
  }

  logout(){
    // if(this.sub){
    //   this.sub.unsubscribe();
    // }
    this.user = null;
    this.userService.logoutUser();
  }
}
