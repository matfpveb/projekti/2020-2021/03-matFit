import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PersonalizedFoodComponent } from './personalized-food.component';

describe('PersonalizedFoodComponent', () => {
  let component: PersonalizedFoodComponent;
  let fixture: ComponentFixture<PersonalizedFoodComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PersonalizedFoodComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PersonalizedFoodComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
