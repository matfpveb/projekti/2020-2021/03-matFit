import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { UserService } from '../services/user.service';
import { Recipe } from '../models/recipe.model';
import { RecipeService } from '../services/recipe.service';


@Component({
  selector: 'app-personalized-food',
  templateUrl: './personalized-food.component.html',
  styleUrls: ['./personalized-food.component.css']
})
export class PersonalizedFoodComponent implements OnInit {

  recipes: Observable<Recipe[]>;

  user = this.userService.getUser();

  constructor(private userService: UserService, private recipeService: RecipeService) { 
     this.recipes= this.recipeService.getPersonalizedSuggestions(this.user.weight, this.user.height, this.user.gender, this.user.age);
  }

  ngOnInit(): void {
 
  }

}

