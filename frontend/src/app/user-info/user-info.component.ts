import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { User } from '../models/user.model';
import { UserService } from '../services/user.service';

@Component({
  selector: 'app-user-info',
  templateUrl: './user-info.component.html',
  styleUrls: ['./user-info.component.css']
})
export class UserInfoComponent implements OnInit, OnDestroy {
  private sub: Subscription;
  public user: User = null;
  public isLogin: boolean = true;

  constructor(private userService: UserService) {
    this.sub = this.userService.user.subscribe((user: User)=>{
      this.user = user;
    });
  }

  ngOnInit(): void {
  }

  ngOnDestroy(): void{
    if(this.sub){
      this.sub.unsubscribe();
    }
  }
}
