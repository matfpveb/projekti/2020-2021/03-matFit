import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { User } from '../models/user.model';
import { UserService } from '../services/user.service';


@Component({
  selector: 'app-signup-form',
  templateUrl: './signup-form.component.html',
  styleUrls: ['./signup-form.component.css']
})
export class SignupFormComponent implements OnInit {
  signUpForm : FormGroup;
  public users;
  public usernameCheck: boolean = false;
  
  createAccountclicked: boolean = false;

  constructor(private fb: FormBuilder, private userService: UserService) { 
    this.userService.getUsers().subscribe(usrs => {
      this.users = usrs;
      console.log(this.users);
    });
  }

  ngOnInit(): void {
    this.signUpForm = this.fb.group({
      username: ['', [Validators.required, Validators.minLength(8)]],
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(8), Validators.maxLength(20), Validators.pattern('^(?=.*[0-9])(?=.*[a-zA-Z])([a-zA-Z0-9]+)$')]],
      repeatPassword: ['', [Validators.required]],
    })
    this.signUpForm.valueChanges.subscribe(console.log);
  }

  public get username(){
    return this.signUpForm.get('username');
  }

  public get email(){
    return this.signUpForm.get('email');
  }
  
  public get password(){
    return this.signUpForm.get('password');
  }

  public get repeatPassword(){
    return this.signUpForm.get('repeatPassword');
  }

  equalPasswords() {
    if(this.password.value !== this.repeatPassword.value){
      return false;
    }
    return true;
  }

   usernameTaken() {
     if(this.users.find(usr => usr.username === this.username.value)){
       this.usernameCheck = true;
     } else {
       this.usernameCheck = false;
     }
  }

  onCreateAccount(){
    this.createAccountclicked = true;
  }
}
