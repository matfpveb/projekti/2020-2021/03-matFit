import { Component, Input, OnInit } from '@angular/core';
import { Recipe } from 'src/app/models/recipe.model';



@Component({
  selector: 'app-recipe-suggestion',
  templateUrl: './recipe-suggestion.component.html',
  styleUrls: ['./recipe-suggestion.component.css']
})
export class RecipeSuggestionComponent implements OnInit {

  @Input() recipe : Recipe;
  clickedRecipe: Recipe;
  show_recipe: boolean;

  onRecipeClick(recipe){
    this.show_recipe=true;
    this.clickedRecipe = recipe;
  }

  closeRecipe(){
    this.show_recipe=false;
  }

  constructor() { }

  ngOnInit(): void {
  }

}
