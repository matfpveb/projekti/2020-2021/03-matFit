import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CategoryEnum } from '../models/recipe.model';
import { Observable } from 'rxjs';
import { Recipe } from 'src/app/models/recipe.model';
import { RecipeService } from 'src/app/services/recipe.service';

declare const $:any;

@Component({
  selector: 'app-food-form',
  templateUrl: './food-form.component.html',
  styleUrls: ['./food-form.component.css']
})
export class FoodFormComponent implements OnInit {

  categoryKeys = Object.values(CategoryEnum).slice(0,6);
  categoryLabels = Object.keys(CategoryEnum).slice(0,6);
  numOfMeals = [2, 3, 4, 5];
  showMealPlan:boolean = false;
  public foodForm: FormGroup;
 
  suggestedRecipes: Observable<Recipe[]>;



  constructor(private formBuilder: FormBuilder, private recipeService: RecipeService) { 

  }

  public goTo(location: string): void {
    window.location.hash = location;
}
  ngOnInit(): void {
    this.foodForm = this.formBuilder.group({
      category: ['anything', [Validators.required]],
      kcalNum: ['1600', [Validators.required, Validators.min(300), Validators.max(4000)]],
      mealNum: ['3', [Validators.required]]
    });
    $('.ui.dropdown').dropdown();
    $('.ui.radio.checkbox').checkbox();
  }

  async onGenerate(){
    const data = this.foodForm.value;
    this.showMealPlan = false;

    if(this.foodForm.invalid){
      window.alert("Please enter between 300 and 4000 calories")
    } else {
      console.log(data.category);
      let recipes: Observable<Recipe[]> = await this.recipeService.getRecipesSuggestions(data.category, data.kcalNum, data.mealNum);
      console.log("recipes", recipes);
      this.suggestedRecipes = recipes;
      this.showMealPlan = true;
    }

  }


}

