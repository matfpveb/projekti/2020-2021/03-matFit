import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UserService } from '../services/user.service';
import { User } from '../models/user.model';
import { Router } from '@angular/router';
import { Observable, Subscription } from 'rxjs';
@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.css']
})
export class SigninComponent implements OnInit, OnDestroy{
signInForm : FormGroup;
public users: User[] = [];
public user: User;
userSub: Subscription;

userExists: boolean = false;

constructor(private router: Router, private fb: FormBuilder, private userService: UserService) {
  this.userService.getUsers().subscribe((usrs: User[]) => {
    this.users = usrs;
    console.log(this.users);
  });
  
}

ngOnInit(): void {
  this.signInForm = this.fb.group({
    username: ['', [Validators.required]],
    password: ['', [Validators.required]],
  });
}
ngOnDestroy(): void {
  if(this.userSub){
    this.userSub.unsubscribe();
  }
}
public get username(){
  return this.signInForm.get('username');
}

public get password(){
  return this.signInForm.get('password');
}

submit(): void{
  const obsUser : Observable <User> = this.userService.loginUser(this.signInForm.value);

  // subscribe() - start emitting values
  const sub: Subscription = obsUser.subscribe((user: User)=>{
    console.log(user);
  });
  this.userSub = sub;
  //window.alert(`User ${this.signInForm.value['username']} has successfully logged in!`);
  //this.router.navigate(['/']); 
}


}
