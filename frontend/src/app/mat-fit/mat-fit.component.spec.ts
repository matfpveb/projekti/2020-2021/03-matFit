import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MatFitComponent } from './mat-fit.component';

describe('MatFitComponent', () => {
  let component: MatFitComponent;
  let fixture: ComponentFixture<MatFitComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MatFitComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MatFitComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
