import { createDirectiveTypeParams } from '@angular/compiler/src/render3/view/compiler';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AboutUsComponent } from './about-us/about-us.component';
import { BrowseFoodsComponent } from './browse-foods/browse-foods.component';
import { CreateRecipeComponent } from './create-recipe/create-recipe.component';
import { HomeComponent } from './home/home.component';
import { SigninComponent } from './signin/signin.component';
import { SignupFormComponent } from './signup-form/signup-form.component';
import { UserInfoComponent } from './user-info/user-info.component';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent
  },
  {
    path: 'signup',
    component: SignupFormComponent
  },
  {
    path: 'signin',
    component: SigninComponent
  },
  {
    path: 'browse',
    component: BrowseFoodsComponent
  },
  {
    path: 'about',
    component: AboutUsComponent
  },
  {
    path: 'userinfo',
    component: UserInfoComponent
  },
  {
    path: 'createRecipe',
    component: CreateRecipeComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
