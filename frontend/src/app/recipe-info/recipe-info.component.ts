import { Component, Input, OnInit } from '@angular/core';
import { Recipe } from 'src/app/models/recipe.model';


@Component({
  selector: 'app-recipe-info',
  templateUrl: './recipe-info.component.html',
  styleUrls: ['./recipe-info.component.css']
})
export class RecipeInfoComponent implements OnInit {

  @Input() recipes : Array<Recipe>;
  clickedRecipe: Recipe;
  show_recipe: boolean;

  onRecipeClick(recipe){
    this.show_recipe=true;
    this.clickedRecipe = recipe;
  }

  closeRecipe(){
    this.show_recipe=false;
  }


  constructor() { }

  ngOnInit(): void {
  }

}
