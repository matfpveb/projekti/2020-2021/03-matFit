import { Component, ElementRef, OnDestroy, OnInit, ViewChild, } from '@angular/core';
import { Subscription } from 'rxjs';
import { IngredientService } from '../services/ingredient.service';
import { IngredientPagination } from '../services/models/ingredient.pagination';

declare const $: any;

@Component({
  selector: 'app-food-search',
  templateUrl: './food-search.component.html',
  styleUrls: ['./food-search.component.css'],
})
export class FoodSearchComponent implements OnInit, OnDestroy {
  @ViewChild('inputName', { static: false }) inputName: ElementRef;
  numOfItems: number;
  numOfPages: number;
  currentPage: number;
  pages: Array<number>;
  sortingHelper: Array<Object>;
  foodList: Array<Object>;
  sub: Subscription;
  name: string;
  sortParam: string;

  constructor(private ingredientService: IngredientService) {
    this.sub = this.ingredientService.getPagination()
      .subscribe((pagination: IngredientPagination) => {
        this.foodList = pagination.docs;
        this.numOfPages = pagination.totalPages;
      });
    this.currentPage = 1;
    this.numOfItems = 10;
    this.pages = [1, 2, 3, 4];
    this.sortParam = 'name';
    this.sortingHelper = [
      { name: true },
      { kcal: true },
      { protein: true },
      { carbohydrates: true },
      { sugar: true },
      { fat: true },
      { saturated: true },
      { cholesterol: true },
    ];
  }
  ngOnDestroy(): void {
    if (this.sub) {
      this.sub.unsubscribe();
    }
  }

  ngOnInit(): void {
    $('.unos').on('keyup', function (e) {
      if (e.key === 'Enter' || e.keyCode === 13) {
        $('.ui.button.hrana').click();
      }
    });

    $('.ui.dropdown').dropdown({
      onChange: (value, text, $selectedItem) => {
        this.numOfItems = parseInt(value);
        $('.ui.button.hrana').click();
      },
    });

    $('.ui.button.hrana').on('click', () => {
      this.currentPage = 1;

      this.name = (this.inputName.nativeElement as HTMLInputElement).value;

      this.sub.unsubscribe();
      this.sub = this.ingredientService
        .getPagination(this.name, this.currentPage, this.numOfItems)
        .subscribe((pagination: IngredientPagination) => {
          this.foodList = pagination.docs;
          this.numOfPages = pagination.totalPages;

          if (this.numOfPages > 4) {
            this.pages = [1, 2, 3, 4];
          } else {
            this.pages = [];
            for (let i = 1; i <= this.numOfPages; i++) {
              this.pages.push(i);
            }
          }
        });
    });
  }

  menuClicked(page) {
    if (Number.isInteger(page)) {
      this.currentPage = page;
      if (this.currentPage > 2 && this.currentPage + 1 <= this.numOfPages) {
        this.pages = [
          this.currentPage - 2,
          this.currentPage - 1,
          this.currentPage,
          this.currentPage + 1,
        ];
      }
    } else if (page == '--') {
      this.currentPage = 1;
      if (this.numOfPages >= 4) {
        this.pages = [1, 2, 3, 4];
      }
    } else if (page == '++') {
      this.currentPage = this.numOfPages;
      if (this.numOfPages >= 4) {
        this.pages = [
          this.numOfPages - 3,
          this.numOfPages - 2,
          this.numOfPages - 1,
          this.numOfPages,
        ];
      }
    } else if (page == '-') {
      if (this.currentPage > 1) {
        this.currentPage -= 1;
        if (this.currentPage > 1 && this.currentPage + 2 <= this.numOfPages) {
          this.pages = [
            this.currentPage - 1,
            this.currentPage,
            this.currentPage + 1,
            this.currentPage + 2,
          ];
        }
      }
    } else if (page == '+') {
      if (this.currentPage < this.numOfPages) {
        this.currentPage += 1;
        if (
          this.currentPage + 2 > this.pages[3] &&
          this.currentPage + 2 <= this.numOfPages
        ) {
          this.pages = [
            this.currentPage - 1,
            this.currentPage,
            this.currentPage + 1,
            this.currentPage + 2,
          ];
        }
      }
    }

    this.sub.unsubscribe();
    this.sub = this.ingredientService
      .getPagination(
        this.name,
        this.currentPage,
        this.numOfItems,
        this.sortParam
      )
      .subscribe((pagination: IngredientPagination) => {
        this.foodList = pagination.docs;
      });
  }

  sortBy(param: string) {
    this.sortParam = param;
    if (!this.sortingHelper[param]) {
      this.sortingHelper[param] = true;
      this.sortParam = '-' + param;
    } else {
      this.sortingHelper[param] = false;
      this.sortParam = param;
    }

    this.sub.unsubscribe();
    this.sub = this.ingredientService
      .getPagination(
        this.name,
        this.currentPage,
        this.numOfItems,
        this.sortParam
      )
      .subscribe((pagination: IngredientPagination) => {
        this.foodList = pagination.docs;
      });
  }
}
