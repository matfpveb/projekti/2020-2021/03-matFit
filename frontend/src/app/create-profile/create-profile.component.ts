import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { User } from '../models/user.model';
import { UserService } from '../services/user.service';
import { Router } from '@angular/router';
import { Observable, Subscription } from 'rxjs';

@Component({
  selector: 'app-create-profile',
  templateUrl: './create-profile.component.html',
  styleUrls: ['./create-profile.component.css']
})
export class CreateProfileComponent implements OnInit, OnDestroy {
  isCheckedMale: boolean;
  isCheckedFemale: boolean;
  isCheckedYes: boolean = false;
  isCheckedNo: boolean = true;
  userSub: Subscription;
  @Input()
  public parentForm: FormGroup;
  public createAccountForm: FormGroup;
  public user: User;
  public users;

  selectedGoal: string = 'lose';
  selectedBodyfat: string = 'medium';

  constructor(private router: Router, private userService: UserService, private fb: FormBuilder) {
    //this.users = this.userService.getUsers();
   }

  ngOnInit(): void {
    this.createAccountForm = this.fb.group({
      username: [this.parentForm.value['username']],
      email: [this.parentForm.value['email']],
      password: [this.parentForm.value['password']],
      firstName: ['', [Validators.required]],
      lastName: ['', [Validators.required]],
      gender: [''],
      age: [''],
      height: [''],
      weight: ['', [Validators.required]],  
    })
  }

  ngOnDestroy(): void {
    if(this.userSub){
      this.userSub.unsubscribe();
    }
  }

  public get firstName(){
    return this.createAccountForm.get('firstName');
  }

  public get lastName(){
    return this.createAccountForm.get('lastName');
  }

  public get weight(){
    return this.createAccountForm.get('weight');
  }

  onCheckedGender(event: Event){
    let value = (<HTMLInputElement>event.target).value;
    if(value == "male"){
      this.isCheckedMale = true;
      this.isCheckedFemale = false;
    } else {
      this.isCheckedMale = false;
      this.isCheckedFemale = true; 
    }
  }

  onCheckedGoal(event: Event) {
    let value = (<HTMLInputElement>event.target).value;
    if(value == "yes") {
      this.isCheckedYes = true;
      this.isCheckedNo = false;
    }
    else {
      this.isCheckedYes = false;
      this.isCheckedNo = true;
    }

  }

  onFinishUp()
  {
    console.log("Form", this.parentForm);
    console.log("Form value", this.parentForm.value);
    console.log("Username value", this.parentForm.value["username"]);
    console.log("createAccountForm username", this.createAccountForm.value['username']);
    console.log("firstName: ", this.createAccountForm.value['firstName']);
    console.log("Gender: ", this.createAccountForm.value['gender']);
    if(this.createAccountForm.invalid){
      window.alert('The form is not valid');
      return;
    }
    const obsUser : Observable<User> = this.userService.addNewUser(this.createAccountForm.value);

    // subscribe() - start emitting values
    const sub: Subscription = obsUser.subscribe((user: User)=>{
      console.log(user);
    });
    this.userSub = sub;
    window.alert(`User with username ${this.parentForm.value['username']} has been created!`);
    this.router.navigate(['/signin']);
  }
}
