import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Observable } from 'rxjs';
import { CategoryEnum, DifficultyEnum, MealEnum, Recipe } from '../models/recipe.model';
import { RecipeService } from '../services/recipe.service';

declare const $:any;

@Component({
  selector: 'app-recipe-search',
  templateUrl: './recipe-search.component.html',
  styleUrls: ['./recipe-search.component.css']
})
export class RecipeSearchComponent implements OnInit {

  recipes: Observable<Recipe[]>;
  clickedRecipe: Recipe;
  show_recipe: boolean;

  @ViewChild('inputName', { static: false }) inputName: ElementRef;
  @ViewChild('inputTime', { static: false }) inputTime: ElementRef;
  filters: Array<string>;
  name: string;
  prep_time_mins: number;
  difficulty: DifficultyEnum;
  category: Array<CategoryEnum>;
  meal: MealEnum;

  closeRecipe(){
    this.show_recipe=false;
  }

  onRecipeClick(recipe){
    this.show_recipe=true;
    this.clickedRecipe = recipe;
  }

  constructor(private recipeService: RecipeService) {
    this.recipes = this.recipeService.getRecipesFiltered(this.name, this.difficulty, this.prep_time_mins, this.meal, this.category);
    this.name = "";
    this.show_recipe = false;
  }

  ngOnInit(): void {
    $('.ui.dropdown').dropdown({
      onChange: function() {
        $('.ui.button').click();
      }
    });


    $('.unos').on('keyup', function (e) {
      if (e.key === 'Enter' || e.keyCode === 13) {
        $('.ui.button').click();
      }
    });
    $('.prep_time_mins').on('keyup', function (e) {
      if (e.key === 'Enter' || e.keyCode === 13) {
        $('.ui.button').click();
      }
    });
  }


  public setFilters() {
    this.name = (this.inputName.nativeElement as HTMLInputElement).value;
    if(this.name == ''){
      this.name = undefined;
    }
    this.category = $('.ui.dropdown.category').dropdown('get value').split(',');
    if(this.category[0] == CategoryEnum.Empty){
      this.category = undefined;
    }
    this.meal = $('.ui.dropdown.meal').dropdown('get value');
    if(this.meal == MealEnum.Empty || this.meal == MealEnum.Any){
      this.meal = undefined;
    }
    this.difficulty = $('.ui.dropdown.difficulty').dropdown('get value');
    if(this.difficulty == DifficultyEnum.Empty || this.difficulty == DifficultyEnum.Any){
      this.difficulty = undefined;
    }

    this.prep_time_mins = parseInt((this.inputTime.nativeElement as HTMLInputElement).value);
    if(isNaN(this.prep_time_mins)){
      this.prep_time_mins = undefined;
    }

    this.recipes = this.recipeService.getRecipesFiltered(this.name, this.difficulty, this.prep_time_mins, this.meal, this.category);
  }

}
