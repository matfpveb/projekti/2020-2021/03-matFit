import { Component, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { User } from './models/user.model';
import { UserService } from './services/user.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnDestroy{
  title = 'MatFit';
  public user: User = null;
  private sub: Subscription;

  constructor(private userService: UserService){
    this.sub = this.userService.user.subscribe((user: User)=>{
      this.user = user;
    })
  }
  
  ngOnDestroy(): void{
    if(this.sub){
      this.sub.unsubscribe();
    }
    
  }
}
