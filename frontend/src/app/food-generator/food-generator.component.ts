import { Component, OnInit } from '@angular/core';
import { UserService } from '../services/user.service';


declare const $:any;



@Component({
  selector: 'app-food-generator',
  templateUrl: './food-generator.component.html',
  styleUrls: ['./food-generator.component.css']
})
export class FoodGeneratorComponent implements OnInit {


  display :boolean;

  constructor(public userService: UserService) {

    if(userService.getUser() != null){
      this.display = true;
    }
  }
  
  isLoggedIn(){
    return this.userService.isLoggedIn();
  }

  ngOnInit(): void {
    $('.personalized').transition('zoom in');
  }

  onPress() {
    this.display = !this.display;
  }


  

}
