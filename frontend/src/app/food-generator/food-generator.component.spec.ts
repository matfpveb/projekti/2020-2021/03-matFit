import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FoodGeneratorComponent } from './food-generator.component';

describe('FoodGeneratorComponent', () => {
  let component: FoodGeneratorComponent;
  let fixture: ComponentFixture<FoodGeneratorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FoodGeneratorComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FoodGeneratorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
