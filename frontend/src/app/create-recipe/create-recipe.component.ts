import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Recipe } from '../models/recipe.model';
import { User } from '../models/user.model';
import { RecipeService } from '../services/recipe.service';
import { UserService } from '../services/user.service';

declare const $:any;


@Component({
  selector: 'app-create-recipe',
  templateUrl: './create-recipe.component.html',
  styleUrls: ['./create-recipe.component.css']
})
export class CreateRecipeComponent implements OnInit {
  public createRecipe: FormGroup;

  private user: User = this.userService.getUser();
  public creator: string = this.user.firstName + " " + this.user.lastName;
  public difficulty: string = '';
  categories: any = ['gluten-free', 'dairy-free','egg-free', 'vegan', 'vegetarian', 'healthy','high-protein','sweet', 'salad', 'paleo', 'keto'];
  mealTypes: any = ['breakfast', 'lunch', 'dinner', 'snack', 'dessert'];

  private imageForUpload : File = null;

  constructor(private recipeService: RecipeService, private userService: UserService, private fb: FormBuilder) {
    this.createRecipe = this.fb.group({
      name: [''],
      category: [''],
      meal: [],
      minutes: [],
      kcal: [],
      proteins: [],
      carbs: [],
      fats: [],
      instructions: [],
      image_url: []
    });
  }

  private getNutrientsObject(){
    return {
      "kcal": this.kcal.value,
      "proteins": this.proteins.value,
      "carbs": this.carbs.value,
      "fats": this.fats.value
    };
  }

  private getArrayOfInstructions(){
    const arr = [];
    arr.push(this.instructions.value);
    return arr;
  }

  get instructions(){
    return this.createRecipe.get('instructions');
  }

  get name(){
    return this.createRecipe.get('name');
  }

  get kcal(){
    return this.createRecipe.get('kcal');
  }

  get proteins(){
    return this.createRecipe.get('proteins');
  }

  get carbs(){
    return this.createRecipe.get('carbs');
  }

  get fats(){
    return this.createRecipe.get('fats');
  }

  get category(){
    return this.createRecipe.get('category');
  }

  get meal(){
    return this.createRecipe.get('meal');
  }
  get minutes(){
    return this.createRecipe.get('minutes');
  }

  get image_url(){
    return this.createRecipe.get('image_url');
  }

  changeCategory(event: any){
    this.category.setValue(event.target.value);
  }

  changeMeal(event: any){
    this.meal.setValue(event.target.value);
  }

  onRecipeDifficulty(value){
    if (value === 'easy'){
      this.difficulty = 'easy'
    } else {
      this.difficulty = 'more effort'
    }
  }

  submitRecipe(){
    var ingredients = [];
    $('.ingredients_wrap div div input').each(function() {
      ingredients.push($(this).val());
    });

    console.log("Ingredients: ", ingredients);

    if(this.createRecipe.invalid){
      window.alert('The form is not valid');
      return;
    }
    console.log("Creator: ", this.creator);
    console.log("Name: ", this.createRecipe.value["name"]);
    console.log("Category: ", this.createRecipe.value['category']);
    console.log("Meal type: ", this.meal.value);
    console.log("Minutes: ", this.createRecipe.value['minutes']);
    console.log("Recipe image: ", this.image_url.value);
    console.log("Form value: ", this.createRecipe.value);
    this.recipeService.addNewRecipe(
       this.name.value,
       this.creator,
       ingredients,
       this.minutes.value,
       this.difficulty,
       this.getArrayOfInstructions(),
       this.category.value,
       this.meal.value,
       this.getNutrientsObject(),
       this.image_url.value
      ).pipe(catchError(err => {
        window.alert('Error while submitting recipe!Please fill out the form correctly!');
        return throwError(err);
      }))
      .subscribe((recipe)=>{
        this.recipeService.putRecipeImage(recipe._id,  this.imageForUpload).subscribe((recipe)=> {
          // this.recipe.image_url = this.image_url;
        });



        window.alert('Your recipe has been added to our database!');
        console.log(recipe)});
  }

  ngOnInit(): void {
    $('.ui.dropdown')
    .dropdown();

    var invalidChars = ["-", "+", "e",];
    this.inputNUmberRestrictions(<HTMLInputElement>document.getElementById('time-input'),invalidChars);
    this.inputNUmberRestrictions(<HTMLInputElement>document.getElementById('kcal-input'),invalidChars);
    this.inputNUmberRestrictions(<HTMLInputElement>document.getElementById('protein-input'),invalidChars);
    this.inputNUmberRestrictions(<HTMLInputElement>document.getElementById('fats-input'),invalidChars);
    this.inputNUmberRestrictions(<HTMLInputElement>document.getElementById('carbs-input'),invalidChars);

    $(document).ready(function() {
      var max_fields = 10;
      var ingredients = $(".ingredients_wrap");
      var add_ingredient = $(".add_ingredient");
      var instructions = $(".instructions_wrap");
      var add_instruction = $(".add_instruction");

      var x = 1;
      $(add_ingredient).click(function(e){
        e.preventDefault();
        if(x < max_fields){
          x++;
          $(ingredients).append('<div class="fields"><div class="fifteen wide field"><input type="text"></div><button class="ui icon button remove_ingredient one wide field"><i class="minus icon"></i></button></div>');
        }
      });

      $(ingredients).on("click",".remove_ingredient", function(e){ //user click on remove text
        e.preventDefault(); $(this).parent('div').remove(); x--;
      });

      var y = 1;
      $(add_instruction).click(function(e){
        e.preventDefault();
        if(y < max_fields){
          y++;
          $(instructions).append('<div class="fields"><div class="fifteen wide field"><textarea rows="5"></textarea></div><button class="ui icon button remove_instruction one wide field" style="height: 38px;"><i class="minus icon"></i></button></div>');
        }
      });

      $(instructions).on("click",".remove_instruction", function(e){ //user click on remove text
        e.preventDefault(); $(this).parent('div').remove(); y--;
      });

    });



    }

    public onChangeFile(event: Event) : void {
      const files: FileList = (event.target as HTMLInputElement).files;
      if(files.length === 0){
        this.imageForUpload = null;
        return;
      }
      this.imageForUpload = files[0];
    }

    inputNUmberRestrictions(element: HTMLInputElement, invalidChars: Array<string>) {

      element.addEventListener("input", function() {
        this.value = this.value.replace(/[e\+\-]/gi, "");
        if (this.value.length > 3)
            this.value = this.value.slice(0,3);
      });

      element.addEventListener("keydown", function(e) {
        if (invalidChars.includes(e.key)) {
          e.preventDefault();
        }
        if (this.value.length > 3)
            this.value = this.value.slice(0,3);
      });
    }

}

