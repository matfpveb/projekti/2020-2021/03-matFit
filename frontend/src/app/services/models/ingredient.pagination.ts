import { Ingredient } from "src/app/models/ingredient.model";

export interface IngredientPagination {
    docs: Ingredient[];
    totalDocs: number;
    limit: number;
    totalPages: number;
    page: number;
    pagingCounter: number;
    hasPrevPage: boolean;
    hasNextPage: boolean;
    prevPage: number;
    nextPage: number | null;
}