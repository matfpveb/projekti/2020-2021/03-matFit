export interface IJWTTokenData {
    id: string;
    username: string;
    email: string;
    firstName: string;
    lastName: string;
    gender: string;
    age: number;
    height: number;
    weight: number;
}