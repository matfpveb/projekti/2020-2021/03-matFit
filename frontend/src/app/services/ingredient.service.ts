import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Ingredient } from '../models/ingredient.model';
import { IngredientPagination } from './models/ingredient.pagination';
import { map } from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})

export class IngredientService {

  constructor(private http: HttpClient) { }

  // public getProducts(name: string = "", page: number = 1, limit: number = 10, sort: string = 'name'): Observable<Ingredient[]>{
  //   const params: HttpParams = new HttpParams()
  //                                       .append('name', name)
  //                                       .append('page', page.toString())
  //                                       .append('limit', limit.toString())
  //                                       .append('sort', sort);
  //   const obs: Observable<IngredientPagination> = this.http.get<IngredientPagination>("http://localhost:3000/api/ingredients", {params});

  //   return obs.pipe(
  //           map((pagination: IngredientPagination) => {
  //             return pagination.docs;
  //           })
  //         );
  // }

  public getPagination(name: string = "", page: number = 1, limit: number = 10, sort: string = 'name'): Observable<IngredientPagination>{
    const params: HttpParams = new HttpParams()
                                        .append('name', name)
                                        .append('page', page.toString())
                                        .append('limit', limit.toString())
                                        .append('sort', sort);
    const obs: Observable<IngredientPagination> = this.http.get<IngredientPagination>("http://localhost:3000/api/ingredients", {params});

    return obs;
  }
}
