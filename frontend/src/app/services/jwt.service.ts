import { Injectable } from '@angular/core';
import { IJWTTokenData } from './models/jwt-token-data';

@Injectable({
  providedIn: 'root'
})
export class JwtService {
  private static readonly USER_TOKEN_ID: string = 'USER_JWT_TOKEN';
  constructor() { }

  public setToken(jwt: string): void {
    // localStorage.setItem(key, value)
    localStorage.setItem(JwtService.USER_TOKEN_ID, jwt);
  }

  public getToken(): string{
    const token: string | null = localStorage.getItem(JwtService.USER_TOKEN_ID);
    if(!token){
      return '';
    }
    return token;
  }

  public getDataFromToken(): IJWTTokenData{
    const token = this.getToken();
    if(token === ''){
      return null;
    }
    // coded jwt
    const payloadString: string = token.split('.')[1];
    // decoding jwt into json
    const userDataJSON: string = window.atob(payloadString);
    // parse json into interface IJWTTokenData
    const payload: IJWTTokenData = JSON.parse(userDataJSON);
    return payload;
  }

  public removeToken(): void{
    localStorage.removeItem(JwtService.USER_TOKEN_ID);
  }
}
