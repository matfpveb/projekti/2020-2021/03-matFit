import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, of, Subject } from 'rxjs';
import { tap, map, catchError } from 'rxjs/operators';
import { User } from '../models/user.model';
import { JwtService } from './jwt.service';
import { IJWTTokenData } from './models/jwt-token-data';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private readonly userSubject : Subject<User> = new Subject<User>();
  // creating stream which will emit value every time userSubject changes its value
  public readonly user: Observable<User> = this.userSubject.asObservable();
  public userObject: User = null;
  public loggedIn: boolean = false;
  private users: Observable<User[]>;
  private readonly usersUrl = {
   getUrl: "http://localhost:3000/api/users/",
   registerUrl: "http://localhost:3000/api/users/register",
   loginUrl: "http://localhost:3000/api/users/login"
  };

  constructor(private http: HttpClient, private jwtService: JwtService, private router: Router) { 
    this.refreshUsers();
  }

  public sendUserDataIfExists() :User {
    const payload: IJWTTokenData = this.jwtService.getDataFromToken();
    if(!payload){
      return null;
    }
    const user: User =  new User(payload.id,
                    payload.username,
                    payload.email,
                    payload.firstName,
                    payload.lastName,
                    payload.gender,
                    payload.age,
                    payload.height,
                    payload.weight);
    this.userObject = user;
    this.userSubject.next(user);
    window.alert("User successfully logged in!");
    this.router.navigate(['/']); 
    return user;
  }

  // GET all users 
  private refreshUsers() : Observable<User[]>
  {
    this.users = this.http.get<User[]>(this.usersUrl.getUrl);
    return this.users;
  }

  public logoutUser(): void{
    // Delete token from localStorage
    this.jwtService.removeToken();
    this.userSubject.next(null);
    this.userObject = null;
  }

  isLoggedIn(){
    if(localStorage.getItem('USER_JWT_TOKEN') != null){
      return true;
    } else {
      return false;
    }
  }

  public loginUser(formValue): Observable<User>{
    const body = {
      "username": formValue['username'],
      "password": formValue['password']
    };
    return this.http.post<{accessToken: string}>(this.usersUrl.loginUrl, body).pipe(
      tap((response: {accessToken: string})=>{
        this.jwtService.setToken(response.accessToken);
      }),
      catchError((error: HttpErrorResponse) => this.handleError(error)),
      map((response: {accessToken: string})=> this.sendUserDataIfExists()),
    );
  }

  // POST new user
  public addNewUser(formValue): Observable<User>{
    const body = {
      "firstName": formValue['firstName'],
      "lastName": formValue['lastName'],
      "username": formValue['username'],
      "email": formValue['email'],
      "password": formValue['password'],
      "gender": formValue['gender'],
      "age": formValue['age'],
      "height": formValue['height'],
      "weight": formValue['weight']
    }
    // transform Observable<string> to Observable<User> by using pipe
    // tap() allows us to get the value that was emmited
    // and do something with that value without changing original stream
    return this.http.post<{accessToken: string}>(this.usersUrl.registerUrl, body).pipe(
     tap((response: {accessToken: string})=>{
       this.jwtService.setToken(response.accessToken);
     }),
     map((response: {accessToken: string})=> this.sendUserDataIfExists())
   );
  }

  public getUser(){
    const payload: IJWTTokenData = this.jwtService.getDataFromToken();
    if(!payload){
      return null;
    }

    return new User(
      payload.id,
      payload.username,
      payload.email,
      payload.firstName,
      payload.lastName,
      payload.gender,
      payload.age,
      payload.height,
      payload.weight);
  }

  public getUsers() :Observable<User[]>{
    return this.users;
  }

  private handleError(error: HttpErrorResponse): Observable<{ token: string }> {
    const serverError: { message: string; status: number; stack: string } = error.error;
    window.alert(`Error: Username or password incorrect!`);
    return of({ token: this.jwtService.getToken() });
  }

}
