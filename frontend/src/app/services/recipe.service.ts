import { JwtService } from './jwt.service';
import { HttpClient, HttpHeaders, HttpParams, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { Recipe } from '../models/recipe.model';
import { map, catchError } from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class RecipeService {
  private readonly addRecipeUrl = "http://localhost:3000/api/recipes/";
  private readonly url = {patchRecipeImage : "http://localhost:3000/api/recipes/uploads"};
  handleError: any;


  constructor(private http: HttpClient, private JwtService: JwtService) {}


  public getRecipesFiltered(name: string = undefined, difficulty: string = undefined, prep_time_mins: number = undefined, meal: string = undefined,category: Array<string> = undefined ): Observable<Recipe[]>{

    var params: HttpParams = new HttpParams();

    if(name != undefined){
        params=params.append('name', name);
    }
    if(difficulty != undefined){
        params=params.append('difficulty', difficulty);
    }
    if(prep_time_mins != undefined){
        params=params.append('prep_time_mins', prep_time_mins.toString());
    }
    if(meal != undefined){
        params=params.append('meal', meal);
    }
    if(category != undefined){
        params=params.append('category', category.toString());
    }

    console.log(params.toString());
    return this.http.get<Recipe[]>("http://localhost:3000/api/recipes/filtered", {params});
  }

  public  getRecipesSuggestions(category: string, kcalNum: number, mealNum: number): Observable<Recipe[]>{
    let params: HttpParams = new HttpParams();

    params = params.append('category', category);
    params = params.append('kcalNum', kcalNum.toString());
    params = params.append('mealNum', mealNum.toString());

    console.log(params.toString());
    return this.http.get<Recipe[]>("http://localhost:3000/api/recipes/suggestion", {params});
  }

  public getPersonalizedSuggestions(weight: number, height: number, gender:string, age: number):  Observable<Recipe[]>{

    let params: HttpParams = new HttpParams();

    params = params.append('weight', weight.toString());
    params = params.append('height', height.toString());
    params = params.append('gender', gender);
    params = params.append('age', age.toString());

    return this.http.get<Recipe[]>("http://localhost:3000/api/recipes/personalized", {params});
  }

  public putRecipeImage(recipeId: string, file: File): Observable<Recipe>{
    const body : FormData = new FormData();
    body.append("file", file);

    //const headers: HttpHeaders = new HttpHeaders().append("Authorization", `Bearer ${this.JwtService.getToken()}`);
    return this.http
      .put<Recipe>(this.url.patchRecipeImage + "/" + recipeId, body) //, { headers })
      // .pipe(
      //   catchError((error: HttpErrorResponse) => this.handleError(error)),
      //   map((response: { token: string }) => console.log(response))
      // );


  }

  public addNewRecipe( name: string,
                       creator: string,
                       ingredients: Array<string>,
                       prep_time_mins: number,
                       difficulty: string,
                       instructions: Array<string>,
                       category: Array<string>,
                       meal: string,
                       nutrients: object,
                       image_url: string
  ): Observable<Recipe> {
    const body = {
      "name": name,
      "creator": creator,
      "ingredients": ingredients,
      "prep_time_mins": prep_time_mins,
      "difficulty": difficulty,
      "instructions": instructions,
      "category": category,
      "meal": meal,
      "nutrients": nutrients,
      "image_url": image_url
    }
   return this.http.post<Recipe>(this.addRecipeUrl, body);
  }





}


