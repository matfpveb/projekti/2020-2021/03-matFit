import { Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { Recipe } from '../models/recipe.model';
declare const $:any;

@Component({
  selector: 'app-view-recipe',
  templateUrl: './view-recipe.component.html',
  styleUrls: ['./view-recipe.component.css']
})
export class ViewRecipeComponent implements OnInit {

  @Output() closeRecipeEvent = new EventEmitter<boolean>();
  @Input() recipe : Recipe;
  @ViewChild('inputNumber', { static: false }) inputNumber: ElementRef;
  numOfPortions : number = 1;

  constructor() {
   }

  ngOnInit(): void {
    (document.scrollingElement as HTMLElement).style.overflow = "hidden";

    this.drawPieChart(this.recipe.nutrients);

    $('body').on('keyup', (e) => {
      if (e.key === 'Escape') {
        (document.scrollingElement as HTMLElement).style.overflow = "";
        this.closeRecipeEvent.emit(false);
      }
    });

    $('.num-of-portions').on('keyup', (e) => {
        this.numOfPortions = parseInt((this.inputNumber.nativeElement as HTMLInputElement).value);
        if(isNaN(this.numOfPortions)){
          this.numOfPortions = 1;
        }
    });

    $('#view-recipe').on('click', (e) => {
      if(e.target == document.getElementById('view-recipe')){
        this.closeRecipeEvent.emit(false);
        (document.scrollingElement as HTMLElement).style.overflow = "";
      }
    });

    $('.x-button').on('click', (e) => {
      if(e.target == document.getElementById('view-recipe')){
        this.closeRecipeEvent.emit(false);
        (document.scrollingElement as HTMLElement).style.overflow = "";
      }
    });


  };

  public onClose(){
    this.closeRecipeEvent.emit(false);
    (document.scrollingElement as HTMLElement).style.overflow = "";
  }


  drawPieChart(nutrients){
    var data = [{
      label: "Protein",
      value: nutrients.protein,
      color: '#976fe8'
    }, {
      label: "Carbs",
      value: nutrients.carbs,
      color: '#FCB524'
    }, {
      label: "Fat",
      value: nutrients.fats,
      color: '#52C0BC'
    }];

    var total = 0;
    for (let obj of data) {
      total += obj.value;
    }

    var canvas: any = document.getElementById('myCanvas');
    var ctx = canvas.getContext('2d');
    var previousRadian;
    var middle = {
      x: (canvas.width / 2),
      y: (canvas.height / 2),
      radius: (canvas.height / 2) - 2,
    };

    for (let obj of data) {

      previousRadian = previousRadian || 0;
      ctx.beginPath();
      ctx.fillStyle = obj.color;
      let radian = (Math.PI * 2) * (obj.value / total);
      ctx.moveTo(middle.x, middle.y);
      ctx.arc(middle.x, middle.y, middle.radius, previousRadian, previousRadian + radian, false);
      ctx.closePath();
      ctx.fill();
      ctx.save();

      let percentage = Math.round(obj.value * 100 / total);
      if (percentage >= 5){
        ctx.translate(middle.x, middle.y);
        ctx.fillStyle = "white";
        ctx.font = 13 + "px Verdana";

        ctx.rotate(previousRadian + radian);
        var labelText = obj.label+ " " + percentage  + "%";
        ctx.fillText(labelText, middle.radius - ctx.measureText(labelText).width -2, -2);
      }

      ctx.restore();
      previousRadian += radian;
    }
  }
}

