import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FoodGeneratorComponent } from './food-generator/food-generator.component';
import { FoodSearchComponent } from './food-search/food-search.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { HomeComponent } from './home/home.component';
import { RecipeSuggestionComponent } from './recepie-suggestion/recipe-suggestion.component';
import { RecipeSearchComponent } from './recipe-search/recipe-search.component';
import { SignupFormComponent } from './signup-form/signup-form.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CreateProfileComponent } from './create-profile/create-profile.component';
import { SigninComponent } from './signin/signin.component';
import { BrowseFoodsComponent } from './browse-foods/browse-foods.component';
import { AboutUsComponent } from './about-us/about-us.component';
import { TeamMembersComponent } from './team-members/team-members.component';
import { ViewRecipeComponent } from './view-recipe/view-recipe.component';
import { FoodFormComponent } from './food-form/food-form.component';
import { ContactUsComponent } from './contact-us/contact-us.component';
import { PersonalizedFoodComponent } from './personalized-food/personalized-food.component';
import { MatFitComponent } from './mat-fit/mat-fit.component';
import { RecipeInfoComponent } from './recipe-info/recipe-info.component';
import { CreateRecipeComponent } from './create-recipe/create-recipe.component';
import { RecipeSuggestionListComponent } from './recipe-suggestion-list/recipe-suggestion-list.component';


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    HomeComponent,
    SignupFormComponent,
    CreateProfileComponent,
    SigninComponent,
    BrowseFoodsComponent,
    AboutUsComponent,
    TeamMembersComponent,
    FoodGeneratorComponent,
    RecipeSuggestionComponent,
    FoodSearchComponent,
    RecipeSearchComponent,
    ViewRecipeComponent,
    FoodFormComponent,
    ContactUsComponent,
    PersonalizedFoodComponent,
    MatFitComponent,
    RecipeInfoComponent,
    CreateRecipeComponent, 
    RecipeSuggestionListComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
