import { AbstractControl, ValidationErrors, ValidatorFn } from "@angular/forms";

export const ContactNameValidator : ValidatorFn = (control: AbstractControl) : ValidationErrors | null => {

  const nameParts: string[] = control.value.trim().split(' ').filter((namePart : string) => namePart.trim().length > 0);

  //ako je korisnik uneo sve razmake
  if(nameParts.length == 0){
    return{
      //greska je 'contactName'
      contactName: { message: "You can't enter only whitespaces!"}
    };
  }

  if(nameParts.every( (namePart:string) => namePart.match(/^[0-9\s]+$/))){
    return{
      contactName: { message: "You can't enter only numbers!"}
    };
  }

  return null;
};
