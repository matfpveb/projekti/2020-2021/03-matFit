import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BrowseFoodsComponent } from './browse-foods.component';

describe('BrowseFoodsComponent', () => {
  let component: BrowseFoodsComponent;
  let fixture: ComponentFixture<BrowseFoodsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BrowseFoodsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BrowseFoodsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
