import { Component, OnInit } from '@angular/core';
import { Ingredient } from '../models/ingredient.model';
@Component({
  selector: 'app-browse-foods',
  templateUrl: './browse-foods.component.html',
  styleUrls: ['./browse-foods.component.css']
})
export class BrowseFoodsComponent implements OnInit {

  searchTypeClass: string;

  constructor() {
    this.searchTypeClass = "recipes";
   }

  ngOnInit(): void {

  }

  typeOfSearchHandler(event: any){
    this.searchTypeClass = event.target.value;
  }
}

