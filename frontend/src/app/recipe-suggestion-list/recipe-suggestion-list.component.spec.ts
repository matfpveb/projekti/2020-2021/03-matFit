import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RecipeSuggestionListComponent } from './recipe-suggestion-list.component';

describe('RecipeSuggestionListComponent', () => {
  let component: RecipeSuggestionListComponent;
  let fixture: ComponentFixture<RecipeSuggestionListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RecipeSuggestionListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RecipeSuggestionListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
