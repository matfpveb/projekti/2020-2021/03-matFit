import { Component, Input, OnInit } from '@angular/core';
import { Recipe } from 'src/app/models/recipe.model';

@Component({
  selector: 'app-recipe-suggestion-list',
  templateUrl: './recipe-suggestion-list.component.html',
  styleUrls: ['./recipe-suggestion-list.component.css']
})
export class RecipeSuggestionListComponent implements OnInit {

  @Input() recipes: Array<Recipe[]>;

  constructor() { }

  ngOnInit(): void {
  }

}
