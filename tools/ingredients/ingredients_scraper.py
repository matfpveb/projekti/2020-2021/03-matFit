from bs4 import BeautifulSoup
import requests
import string
import json


def formatInt(str):
    if str:
        return float(str.replace(',','.'))
    else:
        return None;


if __name__ == '__main__':

    pages = list(string.ascii_uppercase)
    pages[0] = ''

    ingredients = []
    for i in pages:
        URL = 'http://www.foodnutritiontable.com/nutritions/' + i
        page = requests.get(URL)

        soup = BeautifulSoup(page.content, 'html.parser')

        all = soup.find('form').find_next('div').find_next('div').find('div', id='body').find_next('div').find_all('div', {'class': ['vwRow rowheaders','vwRow','vwRow1']})


        for el in all:
            atrs = []
            for atr in el.find_all('div'):
                atrs.append(atr.text.strip())

            ingredients.append({
                'name'              : atrs[0],
                'kcal'              : formatInt(atrs[1]),
                # 'kj'                : formatInt(atrs[2]),
                # 'water'             : formatInt(atrs[3]),
                'protein'           : formatInt(atrs[4]),
                'carbohydrates'     : formatInt(atrs[5]),
                'sugar'             : formatInt(atrs[6]),
                'fat'               : formatInt(atrs[7]),
                'saturated'         : formatInt(atrs[8]),
                # 'monounsaturated'   : formatInt(atrs[9]),
                # 'polyunsaturated'   : formatInt(atrs[10]),
                'cholesterol'       : formatInt(atrs[11]),
                # 'fiber'             : formatInt(atrs[12]),
                # 'emotional'         : formatInt(atrs[14]),
                # 'healthy'           : formatInt(atrs[16])
            })



    with open('../../backend/database/json/ingredients.json', 'w',encoding="utf-8") as outfile:
        json.dump(ingredients, outfile, indent=4, ensure_ascii=False)
