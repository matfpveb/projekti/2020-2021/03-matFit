const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate-v2');


const ingredientSchema = new mongoose.Schema({
        _id: mongoose.Schema.Types.ObjectId,
        name: {
            type: String,
            required: true
        }, 
        kcal: {
            type: Number,
            required: true, 
            min: 0
        },
        protein: {
            type: Number,
            required: true, 
            min: 0
        },
        carbohydrates: {
            type: Number,
            required: true, 
            min: 0    
        },
        sugar: {
            type: Number,
            default: null
        },
        fat: {
            type: Number,
            required: true, 
            min: 0
        },
        saturated: {
            type: Number,
            default: null
        },
        cholesterol: {
            type: Number,
            default: null
        }
});

ingredientSchema.plugin(mongoosePaginate);

const ingredientsModel = mongoose.model('ingredients', ingredientSchema);

async function paginateThroughProducts(name = "", page = 1,limit = 10, sort = 'name') {
    return await ingredientsModel.paginate({$or:[{name}]}, {page, limit, sort});
}

paginateThroughProducts();

module.exports = {ingredientsModel,
                  paginateThroughProducts
                };