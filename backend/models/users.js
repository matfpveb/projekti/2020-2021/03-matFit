// const users = require('../database/users');
// const uuid = require('uuid');
const mongoose = require('mongoose');
const bcrypt = require('bcrypt');

// default = 10; bigger the number of salt rounds
// that longer is going to take to make the hash
// but the password will be more secure
const SALT_ROUNDS = 10;


const usersSchema = new mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    username: {
        type: String,
        required: true
    },
    hashedPassword: {
        type: String,
        required: true
    },
    email: {
        type: String, 
        required: true
    }, 
    status: {
        type: String,
        default: 'active'
    },
    firstName: {
        type: String,
        required: true
    },
    lastName: {
        type: String,
        required: true
    },
    gender: String,
    age: Number,
    height: Number, 
    weight: Number
});

usersSchema.methods.setPassword = async function(password){
    const salt = await bcrypt.genSalt(SALT_ROUNDS);
    this.hashedPassword = await bcrypt.hash(password, salt);
};

usersSchema.methods.isValidPassword = async function (password){
    return await bcrypt.compare(password, this.hashedPassword);
};

// usersModel is set to track users collection in database
const usersModel = mongoose.model('users', usersSchema);
module.exports = usersModel;
