const mongoose = require('mongoose');

var MealType = {BREAKFAST: "breakfast", LUNCH: "lunch", DINNER: "dinner", SNACK: "snack", DESSERT: "dessert"};
Object.freeze(MealType);

function Nutrients(kcal, protein, carbs, fats){
    this.kcal = kcal;
    this.protein = protein;
    this.carbs = carbs;
    this.fats = fats;
};

const recipesSchema = new mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    name:{
        type: String,
        required: true
    },
    creator: {
        type: String,
        required: true
    },
    ingredients:{
        type: [String],
        required: true
    },
    prep_time_mins: Number,
    difficulty: String,
    instructions: {
        type: [String],
        required: true
    },
    category: [String],
    meal: Object.keys(MealType),
    nutrients: Object,
    image_url: String
});

const recipesModel = mongoose.model('recipes', recipesSchema);
module.exports = recipesModel;
