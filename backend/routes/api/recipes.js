const express = require('express');
const router = express.Router();
const controller = require('../../controllers/recipes');

// http://localhost:3000/api/recipes/ ----> get all recipes
router.get('/', controller.getAllRecipes);

// http://localhost:3000/api/recipes/{name} ----> get recipe by name
router.get('/name/:name', controller.getRecipeByName);


router.get('/filtered', controller.getFilteredRecipes);

router.get('/suggestion', controller.getRecipesSuggestions);

router.get('/personalized', controller.getPersonalizedSuggestions);

// http://localhost:3000/api/recipes/difficulty/{difficulty} ----> get recipe by difficulty
//example: http://localhost:3000/api/recipes/difficulty/more effort/
router.get('/difficulty/:difficulty', controller.getRecipesByDifficulty);

// http://localhost:3000/api/recipes/prep_time_mins/{prep_time_mins} ----> get recipe by time
router.get('/prep_time_mins/:prep_time_mins', controller.getRecipesByPrepTime);


// http://localhost:3000/api/recipes/category/{category} ----> get recipe by category
router.get('/category/:category', controller.getRecipesByCategory);


// http://localhost:3000/api/recipes/meal/{meal} ----> get recipe by meal
router.get('/meal/:meal', controller.getRecipesByMealType);


// POST http://localhost:3000/api/recipes/ ---> post new recipe
router.post('/', controller.addNewRecipe);

router.put("/uploads/:recipeId", controller.addRecipesImage);

// DELETE http://localhost:3000/api/recipes/{name} ---->delete recipe by name
router.delete('/:name', controller.deleteRecipe);


module.exports = router;

