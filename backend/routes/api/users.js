const express = require('express');
const controller = require('../../controllers/users');
const authentication = require('../../utils/authentication');

// router for http://localhost:3000/api/users
const router = express.Router();

// GET /api/users/  get all users
// In order to get all users, authentication is needed
// When a user is logged in, accessToken is generated
// user is supposed to use that token for authentication 
router.get('/',/* authentication.isAuthenticated, */controller.getAllUsers);

// GET /api/users/{username}  get user by username
router.get('/:username', controller.getUserByUsername);

// POST /api/users/register add new user
router.post('/register', controller.addNewUser);

// POST /api/users/login user login
router.post('/login', authentication.canAuthenticate, controller.loginUser);

// PUT /api/users/{username} update password by username
router.put('/:username', controller.changePassword);

// DELETE /api/users/{username} delete user by username
router.delete('/:username', controller.deleteUserByUsername);

// error block
// if there is uknown request to this path (for which the router is created)
// return 500 - uknown request
router.use(controller.unknownRequest);

module.exports = router;