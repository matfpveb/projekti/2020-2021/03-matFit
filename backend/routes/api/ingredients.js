const express = require('express');

const router = express.Router();

const ingredientsController = require('../../controllers/ingredients');

router.get('/', ingredientsController.getAllIngredients);
router.post('/', ingredientsController.addNewIngredient);

router.get('/:name', ingredientsController.getIngredientByName);
router.delete('/:name', ingredientsController.deleteIngredient);

module.exports = router;
