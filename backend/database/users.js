const uuid = require('uuid');

const users = [
    {
        id: uuid.v4(),
        username: 'john',
        firstName: 'John',
        lastName: 'Wick',
        email: 'john@matf.bg.ac.rs',
        password: 'john123',
        status: 'active',
        gender: 'male',
        age: 23,
        height: 185,
        weight: 85
    },
    {
        id: uuid.v4(),
        username: 'pavle',
        firstName: 'Pavle',
        lastName: 'Pavlovic',
        email: 'pavle@gmail.com',
        password: 'pavle123',
        status: 'active',
        gender: 'male',
        age: 25,
        height: 192,
        weight: 95
    },
    {
        id: uuid.v4(),
        username: 'maja',
        firstName: 'Maja',
        lastName: 'Markovic',
        email: 'maja@gmail.com',
        password: 'maja123',
        status: 'inactive',
        gender: 'female',
        age: 21,
        height: 164,
        weight: 57
    },
    {
        id: uuid.v4(),
        username: 'klara',
        firstName: 'Klara',
        lastName: 'Lazic',
        email: 'klara@gmail.com',
        password: 'klara123',
        status: 'active',
        gender: 'female',
        age: 23,
        height: 170,
        weight: 64
    },
];

module.exports = users;