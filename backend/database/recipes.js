const uuid = require('uuid');

const recipes = [
    {
        id: uuid.v1(),
        name: 'Pancakes',
        creator: 'Pavle Pavlovic',
        ingredients: ['plain flour', 'eggs', 'milk', 'sunflower', 'lemon', 'sugar'],
        prep_time_mins: 30,
        difficulty: 'easy',
        instructions: 'Put 100g plain flour, 2 large eggs, 300ml milk, 1 tbsp sunflower or vegetable oil and a pinch of salt into a bowl or large jug, then whisk to a smooth batter. Set aside for 30 mins to rest if you have time, or start cooking straight away. Set a medium frying pan over a medium heat and carefully wipe it with some oiled kitchen paper. When hot, cook your pancakes for 1 min on each side until golden, keeping them warm in a low oven as you go.Serve with lemon wedges and caster sugar, or your favourite filling. ',
        category: ['dessert']
    },
    {
        id: uuid.v1(),
        name: 'Mushroom sauce',
        creator: 'Klara Lazic',
        ingredients: ['dried porcini mushrooms', 'butter', 'shallots', 'chestnut mushrooms', 'crème fraîche'],
        prep_time_mins: 25,
        difficulty: 'easy',
        instructions: 'Pour 100ml of boiling water over the dried porcini and leave to stand for 5 mins. In a saucepan, heat the butter and add the shallots, frying gently until they are soft and translucent. Add the chestnut mushrooms and cook for 5 mins more. When they are cooked, pour in 1 tbsp of the liquor from the porcini and discard the rest. Chop the porcini and add them to the pan. Fold in the crème fraîche, bring to a simmer then season.',
        category: ['vegetarian', 'gluten-free']
    },
    {
        id: uuid.v1(),
        name: 'Low-fat moussaka',
        creator: 'Maja Markovic',
        ingredients: ['frozen sliced peppers', 'garlic cloves', 'extra-lean minced beef', 'red lentils', 'dried oregano', 'carton passata', 'aubergine', 'tomatoes', 'olive oil', 'parmesan', 'pot 0% fat Greek yogurt', 'freshly grated nutmeg'],
        prep_time_mins: 55,
        difficulty: 'more effort',
        instructions: 'Cook the peppers gently in a large non-stick pan for about 5 mins – the water from them should stop them sticking. Add the garlic and cook for 1 min more, then add the beef, breaking up with a fork, and cook until brown. Tip in the lentils, half the oregano, the passata and a splash of water. Simmer for 15-20 mins until the lentils are tender, adding more water if you need to.Meanwhile, heat the grill to Medium. Arrange the aubergine and tomato slices on a non-stick baking tray and brush with the oil. Sprinkle with the remaining oregano and some seasoning, then grill for 1-2 mins each side until lightly charred – you may need to do this in batches. Mix half the Parmesan with the yogurt and some seasoning. Divide the beef mixture between 4 small ovenproof dishes and top with the sliced aubergine and tomato. Spoon over the yogurt topping and sprinkle with the extra oregano, Parmesan and nutmeg. Grill for 3-4 mins until bubbling. Serve with a salad, if you like.',
        category: ['healthy', 'lunch', 'dinner']
    },
    {
        id: uuid.v1(),
        name: 'Peanut butter cookies',
        creator: 'John Wick',
        ingredients: ['peanut butter ', 'golden caster sugar', 'salt', 'egg'],
        prep_time_mins: 27,
        difficulty: 'more effort',
        instructions: 'Heat oven to 180C/160C fan/gas 4 and line 2 large baking trays with baking parchment. Measure the peanut butter and sugar into a bowl. Add ¼ tsp fine table salt and mix well with a wooden spoon. Add the egg and mix again until the mixture forms a dough. Break off cherry tomato sized chunks of dough and place, well spaced apart, on the trays. Press the cookies down with the back of a fork to squash them a little. The cookies can now be frozen for 2 months, cook from frozen adding an extra min or 2 to the cooking time. Bake for 12 mins, until golden around the edges and paler in the centre. Cool on the trays for 10 mins, then transfer to a wire rack and cool completely. Store in a cookie jar for up to 3 days.',
        category: ['gluten-free', 'snack', 'dessert']
    },
];

module.exports = recipes;

