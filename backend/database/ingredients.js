const ingredients = [
    {
        id : 1,
        name: "Apple",
        calories: 49.05,
        carbohydrate: 11.83,
        protein: 0.1,
        fat: 1.97
    },
    {
        id : 2,
        name: "Blackberries",
        calories: 25.25,
        carbohydrate: 5.1,
        protein: 0.8,
        fat: 0.1
    },
    {
        id : 3,
        name: "Broccoli",
        calories: 30.8,
        carbohydrate: 1.7,
        protein: 0.9,
        fat: 2.6
    },
    {
        id : 4,
        name: "Salmon",
        calories: 208,
        carbohydrate: 0,
        protein: 20,
        fat: 13
    }
];

module.exports = ingredients ;