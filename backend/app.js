// Express.js module
const path = require('path');
const fs = require('fs');
const express = require('express');
const cors = require('cors'); 
const {urlencoded, json} = require('body-parser');
const mongoose = require('mongoose');

global.__uploadDir = path.join(__dirname, 'uploads');
if (!fs.existsSync(__uploadDir)) {
  fs.mkdirSync(__uploadDir);
}

// Create app by calling express() function
// App is a web server
const app = express();
app.use(cors());

// Connect to the database
// const databaseString = "mongodb://localhost:27017/matFit";
const databaseString = "mongodb+srv://matfija:DAZWzf2dSvCFREIU@matfit.oglqr.mongodb.net/matFit?retryWrites=true&w=majority";

mongoose.connect(databaseString, {
   useNewUrlParser: true,
   useUnifiedTopology: true  
});

mongoose.connection.once('open', function(){
    console.log('Connection successful!');
});

mongoose.connection.on('error', error => {
    console.log('Error: ', error);
});


// Read request as json or key-value pair
app.use(json());
app.use(urlencoded({extended: false}));

// load api as usersAPI
const usersAPI = require('./routes/api/users');
// for paths http://localhost:3000/api/users consult usersAPI 
app.use('/api/users', usersAPI);


const ingredientsAPI = require('./routes/api/ingredients');
app.use('/api/ingredients', ingredientsAPI);


const recipesAPI = require('./routes/api/recipes');
app.use('/api/recipes', recipesAPI);


app.use(function (req, res, next){
    const error = new Error("Uknown request!");
    error.status = 405;
    next(error);
});


app.use(function (error, req, res, next) {
    const statusCode = error.status || 500;
    res.status(statusCode).json({
        error: {
            message: error.message,
            status: statusCode,
            stack: error.stack
        },
    });
});

module.exports = app;