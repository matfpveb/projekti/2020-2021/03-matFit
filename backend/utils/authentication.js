const express = require('express');
const usersService = require('../services/usersServices');
const jwt = require('./jwt');

// Middleware for checking if user has provided username and password
module.exports.canAuthenticate = async (req, res, next) => {
    const username = req.body.username;
    const password = req.body.password;

    try{
        if(!username || !password){
            const error = new Error('Please provide both username and password');
            error.status = 400;
            throw error;
        }

        const user = await usersService.getUserByUsername(username);
        if(!user){
            const error = new Error(`User with username ${username} does not exist!`);
            error.status = 404;
            throw error;
        }

        // If everything's ok, we save data about authorization in request object
        // for example - user id and username from database
        req.userId = user._id; 
        req.username = user.username;
        next();
    } catch(err) {
        next(err);
    }
}

module.exports.isAuthenticated = async (req, res, next) => {
    try{
        // We expect user to send HTTP header like this:
        // "Authorization": "Bearer <JWT>"
        const authHeader = req.header("Authorization");
        if(!authHeader) {
            const error = new Error('You need to pass Authorization header with your request!');
            error.status = 403;
            throw error;
        }
        // Bearer TOKEN
        // we get token like this:
        const token = authHeader.split(' ')[1];
        const decodedToken = jwt.verifyJWT(token);
        if(!decodedToken) {
            const error = new Error('Not Authenticated!');
            error.status = 401;
            throw error;
        }

        // We read data from decoded token and save in req object
        // so the next middleware function can use that data
        req.userId = decodedToken.id;
        req.username = decodedToken.username;
        next();
    } catch(err) {
        next(err);
    }
}