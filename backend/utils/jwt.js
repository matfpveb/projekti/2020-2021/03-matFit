const jwt = require('jsonwebtoken');
require('dotenv').config();

module.exports.generateJWT = (data) => {
    return jwt.sign(data, process.env.ACCESS_TOKEN_SECRET);
};

module.exports.verifyJWT = (token) => {
    return jwt.verify(token, process.env.ACCESS_TOKEN_SECRET);
}