const usersService = require('../services/usersServices');
const validator = require('validator');
const bcrypt = require('bcrypt');

const getAllUsers = async (req, res, next) =>{
    try {
        const users = await usersService.getAllUsers();
        res.status(200).json(users);
    } catch(err){
        next(err);
    }
};

const getUserByUsername = async (req,res, next) =>{
    const username = req.params.username;
    try{
        if(!username){
            const error = new Error('Missing username!');
            error.status = 400;
            throw error;
        } 
        const user = await usersService.getUserByUsername(username);
        if(user) {
            res.status(200).json(user);
        } else {
            res.status(404).json();
        }
    } catch(err) {
        next(err);
    }
};

// When new user is registered, accessToken is returned in response
const addNewUser = async (req, res, next) => {
    const {username, firstName, lastName, email, password, gender, age, height, weight} = req.body;

    try{
        if(!username || !email || !password
           || !firstName || !lastName
          ){
            const error = new Error("Please provide all data for new user!");
            error.status = 400;
            throw error;
        }
        const user = await usersService.getUserByUsername(username);
        if(user) {
            const error = new Error("Username already taken!");
            error.status = 403;
            throw error;
        }
        const accessToken = await usersService.addNewUser(
            username,
            email,
            password,
            firstName,
            lastName,
            gender,
            age,
            height,
            weight
        );

       return res.status(201).json({
        accessToken: accessToken
       });
  
    } catch (error) {
        next(error);
    }
};

const loginUser = async (req, res, next) => {
  const username = req.body.username;
  const password = req.body.password;
  try {
    const user = await usersService.getUserByUsername(username);
    if(user){
      try{
        const validPassword = await bcrypt.compare(password, user.hashedPassword);
        if(validPassword){
          const accessToken = await usersService.getUserJWTByUsername(username);
          return res.status(201).json({
            accessToken: accessToken,
          });
      } 
      }catch(err){
        next(err);
      }
    }
  } catch(err) {
    next(err);
  }
};

const changePassword = async (req, res, next) =>{
    const { username, oldPassword, newPassword } = req.body;

    try {
      if (!username || !oldPassword || !newPassword) {
        const error = new Error('Check forwarded data!');
        error.status = 400;
        throw error;
      }
  
      const user = await usersService.getUserByUsername(username);
  
      if (!user) {
        const error = new Error('Check username and/or passwords!');
        error.status = 404;
        throw error;
      }
  
      const updatedUser = await usersService.changeUserPassword(
        username,
        oldPassword,
        newPassword
      );
  
      if (updatedUser) {
        res.status(200).json(updatedUser);
      } else {
        const error = new Error('Check username and/or passwords!');
        error.status = 403;
        throw error;
      }
    } catch (error) {
      next(error);
    }
  
};


const deleteUserByUsername = async (req, res, next) =>{
    const username = req.params.username;
    try {
      if (!username) {
        const error = new Error('Missing username!');
        error.status = 400;
        throw error;
      }
  
      const user = await usersService.getUserByUsername(username);
      if (!user) {
        const error = new Error('Check username!');
        error.status = 404;
        throw error;
      }
  
      await usersService.deleteUser(username);
      res.status(200).json();
    } catch (error) {
      next(error);
    }
};


// error block
// if there is uknown request to this path (for which the router is created)
// return 500 - uknown request
const unknownRequest = (req, res, next) => {
    const uknownMethod = req.method;
    const uknownPath = req.path;
    res.status(500).json({'message': `Uknown request ${uknownMethod} to ${uknownPath}!`});
};

module.exports = {
    getAllUsers,
    getUserByUsername,
    addNewUser,
    loginUser,
    changePassword,
    deleteUserByUsername,
    unknownRequest
};