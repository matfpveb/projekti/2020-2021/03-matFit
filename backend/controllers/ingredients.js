const ingredientsServices = require('../services/ingredientsServices');

const getAllIngredients = async (req, res, next) => {

    const page = req.query.page;
    const limit = req.query.limit;
    const sort = req.query.sort;
    const name = req.query.name;

    try{
        const ingredients = await ingredientsServices.getAllIngredients(name, page, limit, sort);
        res.status(200).json(ingredients);

    }catch(err){
        next(err);
    }
};

// const getAllIngredients = async (req, res, next) => {

//     try{
//         const ingredients = await ingredientsServices.getAllIngredients();
//         res.status(200).json(ingredients);

//     }catch(err){
//         next(err);
//     }
// };

const getIngredientByName = async (req, res, next) => {
    try{
        //case insensitive
        const ingredient = await ingredientsServices.getIngredientByName(req.params.name);

        if(ingredient == undefined) {
            res.status(404).json();
        }else{
            res.status(200).json(ingredient);
        }

    }catch(err){
        next(err);
    }  
};

const addNewIngredient = async (req, res, next) => {
    const { name, kcal, protein, carbohydrates, sugar, fat, saturated, cholesterol} = req.body;
    if (!name || !kcal || !protein || !carbohydrates || !fat) {
        res.status(400).json();
    }
    else{
        try{
            const newIngredient =  await ingredientsServices.addNewIngredient(name, kcal, protein, carbohydrates, sugar, fat, saturated, cholesterol);
            res.status(201).json(newIngredient);

        }catch(err){
            next(err);
        }        
    }
};

const deleteIngredient = async (req, res, next) => {
    const name = req.params.name;

    try{
        if(!name){
            res.status(400).json();
        }else{
            await ingredientsServices.deleteIngredient(name);
            res.status(200).send();
        }

    }catch(err){
        next(err);
    }
};

module.exports = {
    getAllIngredients,
    getIngredientByName, 
    addNewIngredient,
    deleteIngredient
}
