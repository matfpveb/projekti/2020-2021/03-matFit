const recipesServices = require('../services/recipesServices')
const { uploadFile}  = require('../upload/uploadController')

const getAllRecipes = async (req, res, next) =>{
    try {
        const allRecipes = await recipesServices.getAllRecipes();
        res.status(200).json(allRecipes);
    } catch(err){
        next(err);
    }
};

const getRecipeByName = async (req,res, next) =>{
    try{
        const name = req.params.name;

        if(!name){
            res.status(400).json();
        } else {
            const recipe = await recipesServices.getRecipeByName(name);
            if(recipe) {
                res.status(200).json(recipe);
            } else {
                res.status(404).json();
            }
        }
    } catch(err) {
        next(err);
    }
};

const getFilteredRecipes = async (req,res, next) =>{
    try{
        const name = req.query.name;
        const difficulty = req.query.difficulty;
        const prep_time_mins = req.query.prep_time_mins;
        const category = req.query.category;
        const meal = req.query.meal;


        const recipes = await recipesServices.getFilteredRecipes(name, difficulty, prep_time_mins, category, meal);
        if(recipes) {
            res.status(200).json(recipes);
        } else {
            res.status(404).json();
        }
    } catch(err) {
        next(err);
    }
};


const getRecipesByDifficulty = async (req,res, next) =>{
    try{
        const difficulty = req.params.difficulty;

        if(difficulty == undefined){
            res.status(400).json();
        } else {
            const recipe = await recipesServices.getRecipesByDifficulty(difficulty);
            if(recipe == undefined){
                res.status(404).json();
            }
            else{
                res.status(200).json(recipe);
            }
        }
    } catch(err) {
        next(err);
    }
};


const getRecipesByPrepTime = async (req,res, next) =>{
    try{
        const prep_time_mins = req.params.prep_time_mins;

        if(prep_time_mins == undefined){
            res.status(400).json();
        } else {
            const recipe = await recipesServices.getRecipesByPrepTime(prep_time_mins);
            if(recipe == undefined){
                res.status(404).json();
            }
            else{
                res.status(200).json(recipe);
            }
        }
    } catch(err) {
        next(err);
    
    }
};


const getRecipesByCategory = async (req,res, next) =>{
    try{
        const category = req.params.category;

        if(category == undefined){
            res.status(400).json();
        } else {
            const recipe = await recipesServices.getRecipesByCategory(category);
            if(recipe == undefined){
                res.status(404).json();
            }
            else{
                res.status(200).json(recipe);
            }
        }
    } catch(err) {
        next(err);
    }
};

const getRecipesByMealType = async (req,res, next) =>{
    try{
        const meal = req.params.meal;

        if(meal == undefined){
            res.status(400).json();
        } else {
            const recipe = await recipesServices.getRecipesByMealType(meal);
            if(recipe == undefined){
                res.status(404).json();
            }
            else{
                res.status(200).json(recipe);
            }
        }
    } catch(err) {
        next(err);
    }
};



const addNewRecipe = async (req, res, next) => {
    const { name, creator, ingredients, prep_time_mins, difficulty, instructions, category, meal, nutrients, image_url} = req.body;
    if (!name || !creator || !ingredients || !prep_time_mins|| !difficulty || !instructions || !category || !meal || !nutrients || !image_url) {
        res.status(400).json();
    }
    else{
        try {
            const newRecipe = await recipesServices.addNewRecipe(name, creator, ingredients, prep_time_mins, difficulty, instructions, category, meal, nutrients, image_url);
            res.status(201).json(newRecipe);

            }catch(err){
                next(err);
            }
    }

}; 



const deleteRecipe = async (req, res, next) =>{
    try{
        const name = req.params.name;
        const recipe = await recipesServices.getRecipeByName(name);
        if(recipe) {
            await recipesServices.deleteRecipe(name);
            res.status(200).send();
        } else {
            res.status(404).send();
        }
    } catch(err){
        next(err);
    }
};

const getRecipesSuggestions = async (req, res, next) =>{
    try{
        const category = req.query.category;
        const kcalNum = req.query.kcalNum;
        const mealNum = req.query.mealNum;

        const recipes = await recipesServices.getRecipesSuggestions(category, kcalNum, mealNum);
        if(recipes) {
            res.status(200).json(recipes);
        } else {
            res.status(404).json();
        }
    } catch(err) {
        next(err);
    }
};

const getPersonalizedSuggestions = async(req, res, next) =>{
    try{
        const weight = req.query.weight;
        const height = req.query.height;
        const gender = req.query.gender;
        const age = req.query.age;

        const recipes = await recipesServices.getPersonalizedSuggestions(weight, height, gender, age);
        if(recipes) {
            res.status(200).json(recipes);
        } else {
            res.status(404).json();
        }
    } catch(err) {
        next(err);
    }
}

const addRecipesImage = async (req, res, next) => {
   
    const recipeId = req.params.recipeId;
    try {
      await uploadFile(req, res);  
      if (req.file == undefined) {
        const error = new Error('Please upload a file!');
        error.status = 400;
        throw error;
      }
  
      const image_url = req.file.filename;

      const recipe = await recipesServices.addRecipesImage(recipeId, image_url);
      if(recipe) {
        res.status(200).json(recipe);
        } 
      else {
        res.status(404).json();
       }
    } catch (err) {
      next(err);
    }
  };



  

module.exports = {
    getAllRecipes,
    getRecipeByName,
    getFilteredRecipes,
    getRecipesSuggestions,
    getPersonalizedSuggestions,
    getRecipesByDifficulty,
    getRecipesByPrepTime,
    getRecipesByCategory,
    getRecipesByMealType,
    addNewRecipe,
    deleteRecipe,
    addRecipesImage
};


  
