const mongoose = require('mongoose');
const Recipe = require('../models/recipes');


const getAllRecipes = async () =>{

    const allRecipes = await Recipe.find({}).exec();
    return allRecipes;
};

const getRecipeByName = async (name) =>{

    const recipe = await Recipe.find({name: new RegExp(name, "i")}).exec();
    return recipe;

};

const getFilteredRecipes = async (name, difficulty, prep_time_mins, category, meal) => {

    let params = {}

    if(name != undefined){
        params["name"] = new RegExp(name, "i");
    }
    if(meal != undefined){
        params["meal"] = meal;
    }
    if(difficulty != undefined){
        params["difficulty"] = difficulty;
    }
    if(prep_time_mins != undefined){
        params["prep_time_mins"] = { "$lte" : prep_time_mins};
    }
    if(category != undefined){
        params["category"] = {"$all" : category.split(',')};
    }
    const recipes = await Recipe.find(params).exec();
    return recipes;
}


const getRecipesByDifficulty = async (difficulty) =>{

    const recipe = await Recipe.find({difficulty: difficulty}).exec();
    return recipe;
};


const getRecipesByPrepTime = async (prep_time_mins) =>{

    const recipe = await Recipe.find({prep_time_mins: {$lt : prep_time_mins+1}}).exec();
    return recipe;
};


const getRecipesByCategory = async (category) =>{

    const recipe = await Recipe.find({category: category}).exec();
    return recipe;
};


const getRecipesByMealType = async (meal) =>{

    const recipe = await Recipe.find({meal: meal}).exec();
    return recipe;
};



const addNewRecipe = async (name, creator, ingredients, prep_time_mins, difficulty, instructions, category, meal, nutrients, image_url) => {

    const newRecipe = new Recipe({
        _id: new mongoose.Types.ObjectId(), 
        name: name,
        creator: creator,
        ingredients: ingredients,
        prep_time_mins: prep_time_mins,
        difficulty: difficulty,
        instructions: instructions,
        category: category,
        meal: meal,
        nutrients: nutrients,
        image_url: image_url
    });
            
    await newRecipe.save();
    return newRecipe;
};



const deleteRecipe = async (name) =>{

    await Recipe.findOneAndDelete({name: name}).exec();
};
const mealMaps = {
    2: {
        0: "breakfast",
        1: "dinner"    
    },
    3: {
        0: "breakfast",
        1: "lunch",
        2: "dinner"
    }, 
    4: {
        0: "breakfast",
        1: "lunch",
        2: "snack",
        3: "dinner"
    }, 
    5: {
        0: "breakfast",
        1: "snack",
        2: "lunch",
        3: "snack",
        4: "dinner"
    }
}
const getRecipesSuggestions = async (category, kcalNum, mealNum) =>{
    console.log("getRecipesSuggestions", category, kcalNum, mealNum);
    let recipePromises = [];

    const findRandomMeal = (meal, maxKcal) => {   
        let query = {meal};
        if (category.toLowerCase() != "anything"){
            query = {
                meal,
                category :  category.toLowerCase()
            }
        }     
        let promise = Recipe.find(query).where("nutrients.kcal").lte(maxKcal).exec()
        .then((res) => {
            if (res.length == 0){
                return Promise.resolve({
                    meal
                });
            }
            //returns random element from res array
            return Promise.resolve(res[Math.floor(Math.random() * res.length)]);
        });
        recipePromises.push(promise);
    }

    let kcalPerMeal = kcalNum / mealNum;
    for (let i=0; i<mealNum; i++){
        findRandomMeal(mealMaps[mealNum][i], kcalPerMeal);
    }

    return Promise.all(recipePromises);
};

const getPersonalizedSuggestions = async(weight, height, gender, age) => {
    let category = "Anything"; //user can have this in his model
    let kcalNum = (gender == "male")?
        10*weight + 6.25*height - 5*age + 5
        :
        10*weight + 6.25*height - 5*age - 161;
    let mealNum = 4;
    return getRecipesSuggestions(category, kcalNum, mealNum);
}

async function addRecipesImage(recipeId, image_url) {
    const recipe = await Recipe.findOne({_id: recipeId}).exec();
    recipe.image_url = image_url;
    await recipe.save();
    return recipe;
  }
  
  


module.exports = {
    getAllRecipes,
    getRecipeByName,
    getFilteredRecipes,
    getRecipesSuggestions,
    getPersonalizedSuggestions,
    getRecipesByDifficulty,
    getRecipesByPrepTime,
    getRecipesByCategory,
    getRecipesByMealType,
    addNewRecipe,
    deleteRecipe,
    addRecipesImage
};
