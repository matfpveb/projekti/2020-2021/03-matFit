const mongoose = require('mongoose');
const Ingredient = require('../models/ingredients');

// const getAllIngredients = async () => {
//     const ingredients = await Ingredient.find({}).exec();
//     return ingredients;

// };

const getAllIngredients = async (name, page, limit, sort) => {

    const ingredients = await Ingredient.paginateThroughProducts(new RegExp(name, "i"), page, limit, sort);
    return ingredients;

};

const getIngredientByName = async (name) => {
    const ingredient = await Ingredient.paginateThroughProducts(new RegExp(name, "i"),1,100,"name");
    return ingredient.docs;
};

const addNewIngredient = async (name, kcal, protein, carbohydrates, sugar, fat, saturated, cholesterol) => {

    const newIngredient = new Ingredient ({
        _id: new mongoose.Types.ObjectId(),
        name: name,
        kcal: kcal,
        protein: protein,
        carbohydrates: carbohydrates,
        sugar: sugar,
        fat: fat, 
        saturated: saturated,
        cholesterol: cholesterol
    });

    await newIngredient.save();
    return newIngredient;
};

const deleteIngredient = async (name) => {
    await Ingredient.findOneAndDelete({name: name}).exec();
};

module.exports = {
    getAllIngredients,
    getIngredientByName, 
    addNewIngredient,
    deleteIngredient
}