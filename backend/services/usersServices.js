const mongoose = require('mongoose');
const User = require('../models/users');
const jwtUtil = require('../utils/jwt');

const getAllUsers = async () => {
    const users = await User.find({}).exec();
    return users;
};

const getUserByUsername = async (username) => {
    const user = await User.findOne({username: username}).exec();
    return user;
};

const getUserJWTByUsername = async (username) => {
    const user = await getUserByUsername(username);
    if(!user){
        throw new Error(`User with username ${username} does not exist!`);
    }
    const userObject = {
        id: user.id,
        username: user.username,
        email: user.email,
        firstName: user.firstName,
        lastName: user.lastName,
        gender: user.gender,
        age: user.age,
        height: user.height,
        weight: user.weight
    };
    return jwtUtil.generateJWT(userObject);
};


const addNewUser = async (
    username,
    email,
    password,
    firstName,
    lastName,
    gender,
    age,
    height,
    weight ) => {
    const newUser = new User();
    newUser._id = new mongoose.Types.ObjectId();
    newUser.username = username;
    newUser.email = email;
    newUser.password = await newUser.setPassword(password);
    newUser.firstName = firstName;
    newUser.lastName = lastName;
    newUser.gender = gender;
    newUser.age = age;
    newUser.height = height;
    newUser.weight = weight;

    await newUser.save();
    return getUserJWTByUsername(username);
};

const changeUserPassword = async (username, oldPassword, newPassword) => {
    const updatedUser = await User.findOneAndUpdate(
        { username: username },
        { $set: {password: newPassword}},
        { new: true}
        );
    return updatedUser;
};

const deleteUser = async(username) => {
    await User.findOneAndDelete({username: username}).exec();
};

module.exports = {
    getAllUsers,
    getUserByUsername,
    addNewUser,
    changeUserPassword,
    deleteUser,
    getUserJWTByUsername,
};
    