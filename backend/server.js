//http module
const http = require('http');
const app = require('./app');

//defining port
const port = process.env.PORT || 3000;

//creating a server
const server = http.createServer(app);

//initializing listener
server.listen(port, () => {
    console.log(`App is running on http://localhost:${port}`);
});