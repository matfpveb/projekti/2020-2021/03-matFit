# Project matFit

Web application which allows users to generate personalized meal plan in order to achieve their goals in a healthy way.

## How to run
Clone git repository to your local machine.

Server:
```bash
cd backend
npm install
npm start 
```

Client:
```bash
cd frontend
npm install
ng serve
```

## Database matFit

<table>
<tr>
<th>Ingredients</th>
<th>Recipes</th>
<th>Users</th>
</tr>
<tr>
<td>

Name          | Type     | Required |
--------------|----------|----------|
_id           | ObjectId | Auto     |
name          | String   | Yes      | 
kcal          | Int      | Yes      |
protein       | Int      | Yes      |
carbohydrates | Int      | Yes      |
sugar         | Int      | No       |
fat           | Int      | Yes      |
saturated     | Int      | No       |
cholesterol   | Int      | No       |

</td>
<td>

Name           | Type             | Required |
---------------|------------------|----------|
_id            | ObjectId         | Auto     |
name           | String           | Yes      | 
creator        | String           | Yes      |
Ingredients    | Array\<String\>  | Yes      |
prep_time_mins | Int              | Yes      |
difficulty     | String           | Yes      |
instructions   | Array\<String\>  | Yes      |
category       | Array\<String\>  | No       |
meal           | String           | Yes      |
nutrients      | Object           | Yes      |
image_url      | String           | Yes      |

</td>
<td>

Name            | Type              | Required  |
----------------|-------------------|-----------|
_id             | ObjectId          | Auto      |
username        | String            | Yes       |
hashedPassword  | String            | Yes       |
email           | String            | Yes       |
firstName       | String            | Yes       |
lastName        | String            | Yes       |
status          | String            | Yes       |
gender          | String            | Yes       |
age             | Int               | Yes       |
height          | Int               | Yes       |
weight          | Int               | Yes       |
__v             | Int               | Auto      |

</td>
</tr>
</table>

## Demo presentation

Our project is divided into four main components, **home**, **browse foods**, **about us** and **sign in/sign up**.<br>
In the following, we will explain these in more detail.

### About us

Here you can see **What is MatFit**, learn about our **Team members** and even **Contact us** if you have any complaints or suggestions.<br>
![](videos/Recording01.mp4)

### Browse foods

At this page(component) we have two main child components, **Browse foods** and **Browse recipes**.<br>
They are two entities which are independent.<br>

#### *Browse foods*

This component displays all the ***ingredients*** that we have in our database.<br><br>
Elements of this component:
- **Search input** : filters ingredients by a given input string *(it doesn't have to match, just contain the string)*
- **Show this much dropdown** : choose how much items you want to be shown on the page
- **Search button** : press to show with new preferences *(automatically pressed on dropdown, can also be pressed with ENTER on search input)*
- **Table of content** : displayed ingredients *(table is sortable by all table headers, click multiple times to alternate between ascending and descending)*
- **Pagination buttons** : browse through pages with ease<br><br>
![](videos/Recording02.mp4)

#### *Browse recipes*

This component displays all the ***recipes*** that we have in our database.<br><br>
Elements of this component:
- **Search input** : filters recipes by a given input string *(it doesn't have to match, just contain the string)*
- **Meal type dropdown** : select what type of meal you would like to search *(breakfast, lunch, dinner, snack, desert)*
- **Difficulty dropdown** : select level of difficulty *(easy, more effort)*
- **Prep time input** : select the maximum preperation time in minutes
- **Search button** : press to show with new preferences *(automatically pressed on dropdown, can also be pressed with ENTER on search input)*
- **Category dropdown** : filter through one or more categories
- **Card list** : one card represents one recipe<br><br>
![](videos/Recording03.mp4)
- **View recipe** : when recipe is clicked this component pops up, this pop up component contains all recipe information
    - ingredients and instructions are listed down
    - on the top right side you can see a pie graph which is generated based on recipe nutrients
    - changing the ***Amount*** value, you can observe the changes in the nutrients table
    - in the bottom right corner there are pictures that correspond to the category list in which this recipe belongs to
    - you can close this component by clicking on X button in the top right corner or outside the box<br><br>
![](videos/Recording04.mp4)

### Home

This is the root of our project. Here we create personalized meal plans based on your preferences.<br>
There meal plans are made of recipes from our database.<br><br>
Elements of this component:
- **Category buttons** : choose your diet/category
- **Kcal input** : choose the number of calories you want to consume throughout the day
- **Number of meals dropdown** : choose the number of meals
- **Generate button** : generate your plan
- **Recipe list** : generated meals for the day<br><br>
![](videos/Recording05.mp4)
- **Daily recommendation component** : this component generates only when you're logged in
    - this component looks like recipe search page that contains four recipes and also ***view recipe*** is implemented
    - recipes are generated based on your profile info *(weight, height, motive, category preferences)*<br><br>
![](videos/Recording06.mp4)

### Log in/Sign up

#### *Sign up form*

Here new user can create account. Every field is required and you need to make sure validation rules apply or you wont be able to continue.<br>
After meeting all conditions you will be prompted to a new form where you need to enter your information.<br>
After you fill out everything new user will be added to database and you will be redirected to log in page.<br>
![](videos/Recording07.mp4)

#### *Log in form*

In order to successfully log in, your account needs to exist in database.<br>
When you log in, ***log in/sign up*** buttons will disappear and new ones will appear, ***Create recipe*** and ***Log out***.<br>
Refreshing the page or exiting the browser will not log you out, you need to click ***Log out*** button.
![](videos/Recording08.mp4)

#### *Create recipe*

***Create recipe*** will allow user to add a recipe to the database.<br>
All fields are required.<br>
After adding new recipe to the database, you can view it at ***Browse recipes***.<br>
![](videos/Recording09.mp4)<br>
`TODO: need to fix bugs with image upload`




## Developers

- [Nevena Soldat, 504/2017](https://gitlab.com/NevenaSoldat)
- [Ana Miloradovic, 487/2017](https://gitlab.com/ana.miloradovic)
- [Saska Keneski, 298/2016](https://gitlab.com/keneshki)
- [Luka Jokanovic, 197/2016](https://gitlab.com/rogue97)
